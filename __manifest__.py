# -*- coding: utf-8 -*-
{
    'name': "Test Project",
    'category': 'Extra Tools',
    'version': '1.0',
    'summary': "Module for learning odoo",
    'sequence': '10',
    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",
    'depends': ['sale','hr'],

    # always loaded
    'data': [
        'views/assets.xml',
        'views/employee_view_inherit.xml',
        'reports/report_wsy_test.xml',
    ],
    'css':
        ['static/src/css/template_employee_ar.css'],
    'installable': True,
    'application': False,
    'auto_install': False,
}