# -*- coding: utf-8 -*-

from odoo import models, fields, api


class EmployeeInherit(models.Model):
    _inherit = 'hr.employee'

    employee_name_ar = fields.Char(string="الاسم بالعربي")
